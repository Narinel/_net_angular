﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web123.Models
{
    public class UserDetail
    {
        [Key]
        public int PMId { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string UserName { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string Position { get; set; }

        
        [Column(TypeName = "date")]
        public DateTime? StartVacationDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndVacationDate { get; set; }
    }
}
