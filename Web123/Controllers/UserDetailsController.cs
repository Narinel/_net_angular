﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web123.Models;

namespace Web123.Controllers
{
    // for work with controller need migration database from nuget or cmd command as add-migration {name}
    //and create Table from command update-database
    [Route("api/[controller]")]
    [ApiController]
    public class UserDetailsController : ControllerBase
    {
        private readonly UserDetailsContext _context;

        public UserDetailsController(UserDetailsContext context)
        {
            _context = context;
        }

        // GET: api/UserDetails
        [HttpGet]
        public IEnumerable<UserDetail> GetUsersDetails()
        {
            
            return _context.UsersDetails;
        }

        
        

        // GET: api/UserDetails/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserDetail([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userDetail = await _context.UsersDetails.FindAsync(id);

            if (userDetail == null)
            {
                return NotFound();
            }

            return Ok(userDetail);
        }

        // PUT: api/UserDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUserDetail([FromRoute] int id, [FromBody] UserDetail userDetail)
        {

            

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userDetail.PMId)
            {
                return BadRequest();
            }

            int QAVacation = _context.UsersDetails.Where(t => t.Position == "QA")
                .Where(e => userDetail.StartVacationDate <= e.EndVacationDate)
                .Where(d => userDetail.EndVacationDate >= d.StartVacationDate).Count();

            int DevVacation = _context.UsersDetails.Where(t => t.Position == "Developer")
                            .Where(e => userDetail.StartVacationDate <= e.EndVacationDate)
                            .Where(d => userDetail.EndVacationDate >= d.StartVacationDate).Count();

            int TeamLeadVacation = _context.UsersDetails.Where(t => t.Position == "TeamLead")
                            .Where(e => userDetail.StartVacationDate <= e.EndVacationDate)
                            .Where(d => userDetail.EndVacationDate >= d.StartVacationDate).Count();

            if (userDetail.Position == "QA")
            {
                if (DevVacation < 2)
                {
                    if (TeamLeadVacation < 2)
                    {
                        if (QAVacation < 3 && DevVacation < 2)
                        {
                            _context.Entry(userDetail).State = EntityState.Modified;
                            try
                            {
                                await _context.SaveChangesAsync();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!UserDetailExists(id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                        }
                        else if (QAVacation < 4)
                        {
                            _context.Entry(userDetail).State = EntityState.Modified;
                            try
                            {
                                await _context.SaveChangesAsync();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!UserDetailExists(id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            else if (userDetail.Position == "Developer")
            {
                if (QAVacation < 3)
                {
                    if (TeamLeadVacation < 2)
                    {
                        if (DevVacation < 1 && QAVacation< 3)
                        {
                            _context.Entry(userDetail).State = EntityState.Modified;
                            try
                            {
                                await _context.SaveChangesAsync();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!UserDetailExists(id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                        }
                        else if (DevVacation < 3)
                        {
                            _context.Entry(userDetail).State = EntityState.Modified;
                            try
                            {
                                await _context.SaveChangesAsync();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!UserDetailExists(id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    else
                    {
                        return BadRequest();
                    }
                }
                else
                {
                    return BadRequest();
                }
            }
            else if (userDetail.Position == "TeamLead")
            {
                if (DevVacation < 3)
                {
                    if (TeamLeadVacation < 2) 
                    {
                        
                            _context.Entry(userDetail).State = EntityState.Modified;
                            try
                            {
                                await _context.SaveChangesAsync();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!UserDetailExists(id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                    }
                    else
                    {
                        return BadRequest();
                    }

                }
                else
                {
                    return BadRequest();
                }
            }



            return NoContent();
            
        }

        // POST: api/UserDetails
        [HttpPost]
        public async Task<IActionResult> PostUserDetail([FromBody] UserDetail userDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

          
            _context.UsersDetails.Add(userDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUserDetail", new { id = userDetail.PMId }, userDetail);
        }

        // DELETE: api/UserDetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserDetail([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userDetail = await _context.UsersDetails.FindAsync(id);
            if (userDetail == null)
            {
                return NotFound();
            }

            _context.UsersDetails.Remove(userDetail);
            await _context.SaveChangesAsync();

            return Ok(userDetail);
        }

        private bool UserDetailExists(int id)
        {
            return _context.UsersDetails.Any(e => e.PMId == id);
        }

       /* private bool Vacation(int id,string Position,string FC, string SC)
        {

            var PositionUser = _context.UsersDetails.Where(t => t.Valitation == Position);

            PositionUser.

            return ;
        }*/
    }
}