import { Component, OnInit } from '@angular/core';
import { UserDetailService } from 'src/app/shared/user-detail.service';
import { NgForm } from '@angular/forms';
import { Toast, ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { UserDetailListComponent } from '../user-detail-list/user-detail-list.component';
import { UserDetail } from 'src/app/shared/user-detail.model';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styles:[]
})
export class UserDetailComponent implements OnInit {
  selectedOption:string;
  readonly rootURL = 'https://localhost:44394/api';


  public options = [
    { name: "QA", value: 1 },
    { name: "Developer", value: 2 },
    { name: "TeamLead",value:3 }
  ]

  constructor(public service:UserDetailService,
    private toastr: ToastrService,
    private https:HttpClient,
    ) {
   }

  ngOnInit(): void {
    
    this.resetForm()
  }

  resetForm(form?:NgForm){
    if (form != null )
      form.resetForm();
    this.service.formData = {
        PMId: 0,
        UserName:'',
        Position:'',
        StartVacationDate: '',
        EndVacationDate: ''
    }
  }

  onSubmit(form:NgForm){
    
    
    if (this.service.formData.PMId==0){
        
      this.insertRecord(form);

    }
    else{
      
      }
      
  }

  insertRecord(form:NgForm){
    
    this.service.postUserDetail(form.value).subscribe(
      res => {
        this.resetForm(form);
        
        this.toastr.success('Submit Successfuly','User Register');
        this.service.refreshList();
         
        
      },
      err=>{
        console.log('не отправил');
      })
      
  }
  updateRecord(form: NgForm){
    
    this.service.putUserDetail(form.value).subscribe(
      res => {
        this.resetForm(form);
  
        this.toastr.info('Submit Successfuly','User Update');
        this.service.refreshList();
        
      },
      err=>{
        console.log(err);
      })
      
  }

  
}
