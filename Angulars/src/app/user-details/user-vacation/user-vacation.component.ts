import { Component, OnInit } from '@angular/core';
import { UserDetailService } from 'src/app/shared/user-detail.service';
import { NgForm } from '@angular/forms';
import { Toast, ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { UserDetailListComponent } from '../user-detail-list/user-detail-list.component';
import { UserDetail } from 'src/app/shared/user-detail.model';



@Component({
  selector: 'app-user-vacation',
  templateUrl: './user-vacation.component.html',
  styles: []
})
export class UserVacationComponent implements OnInit {


  constructor(public service:UserDetailService,
              
    
    private toastr: ToastrService,
    private https:HttpClient,
    ) {
    
  }
  ngOnInit(): void {
  }

  updateRecord(form: NgForm){
    
    this.service.putUserDetail(form.value).subscribe(
      res => {
        this.resetForm(form);
        
        this.toastr.info('Submit Successfuly','User Update');
        this.service.refreshList();
        
      },
      err=>{
        console.log(err);
        this.toastr.info('Vacation failed','User not Update');
      })
      
  }

  resetForm(form?:NgForm){
    if (form != null )
      form.resetForm();
    this.service.formData = {
        PMId: 0,
        UserName:'',
        Position:'',
        StartVacationDate: '',
        EndVacationDate: ''
    }
  }
}
