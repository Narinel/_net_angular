import { Component, OnInit } from '@angular/core';
import { UserDetailService } from 'src/app/shared/user-detail.service';
import { UserDetail } from 'src/app/shared/user-detail.model';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-user-detail-list',
  templateUrl: './user-detail-list.component.html',
  
  
  styles:[]
})
export class UserDetailListComponent implements OnInit {
   

  constructor(public service: UserDetailService,
    private toastr: ToastrService) { }

    th = document.getElementsByTagName('th');
    

  ngOnInit(): void {
    
    

    for (let c = 0; c < this.th.length; c++) {
      const element = this.th[c];
      this.th[c].addEventListener('click',item(c));
    }
    this.service.refreshList();
    
    function item(c: number){

      return function(){
        sortTable(c);
      }
    }
    

    function sortTable(c) {
      var table, rows, switching, i, x, y, shouldSwitch;
      table = document.getElementById("myTable");
      switching = true;
      while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
          shouldSwitch = false;
          x = rows[i].getElementsByTagName("TD")[c];
          y = rows[i + 1].getElementsByTagName("TD")[c];
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            
            break;
          }
        }
        if (shouldSwitch) {
          
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
        }
      }
    }
  }

  populateForm(item:UserDetail){
    
    this.service.formData = Object.assign({},item);
    
  }

  

  onDelete(PMId){
    if(confirm('Are you sure?')){
      
      this.service.deleteUserDetail(PMId)
      .subscribe(res =>{
      this.service.refreshList();

      this.toastr.warning('Delete Succesfully','Deleted User');
      },
      err=>{
        console.log(err);
      })
    }
  }

  
  
}

  

