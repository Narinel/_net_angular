import { Injectable } from '@angular/core';
import { UserDetail } from './user-detail.model';
import { HttpClient } from '@angular/common/http';
import { UserDetailComponent } from '../user-details/user-detail/user-detail.component';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class UserDetailService {
  formData:UserDetail;
  readonly rootURL = 'https://localhost:44394/api';
  list: UserDetail[];
  absc: UserDetail[];

  constructor(private https:HttpClient, 
    ) { }

  

 
  
  postUserDetail(formData:UserDetail ){
    
    return this.https.post(this.rootURL+'/UserDetails', this.formData);
  }
  putUserDetail(formData:UserDetail ){
    return this.https.put(this.rootURL+'/UserDetails/'+ this.formData.PMId,this.formData);
  }
  deleteUserDetail(id){
    return this.https.delete(this.rootURL+'/UserDetails/'+ id);
  }

  

  refreshList(){
    
    document.getElementById('Vacation').style.display='none';
    document.getElementById('UsersReg').style.display='block';
    this.https.get(this.rootURL+'/UserDetails')
    .toPromise()
    .then(res=>this.list = res as UserDetail[]);
    
    
    
    
  }

  

}
