import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { ToastrModule } from 'ngx-toastr'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserDetailComponent } from './user-details/user-detail/user-detail.component';
import { UserDetailListComponent } from './user-details/user-detail-list/user-detail-list.component';
import { UserDetailService } from './shared/user-detail.service';
import { HttpClientModule } from "@angular/common/http";
import { UserVacationComponent } from './user-details/user-vacation/user-vacation.component';


@NgModule({
  declarations: [
    AppComponent,
    UserDetailsComponent,
    UserDetailComponent,
    UserDetailListComponent,
    UserVacationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [UserDetailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
